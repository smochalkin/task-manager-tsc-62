package ru.smochalkin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IRepositoryDto;
import ru.smochalkin.tm.dto.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IRepositoryDto<TaskDto> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<TaskDto> findAll();

    @NotNull
    List<TaskDto> findAllByUserId(@Nullable String userId);

    @NotNull
    TaskDto findById(@Nullable String id);

    @Nullable
    TaskDto findByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @Nullable
    TaskDto findByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    @Nullable
    TaskDto findByName(
            @Nullable String userId, @NotNull String name
    );

    void removeById(@Nullable String id);

    void removeByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeByName(
            @Nullable String userId, @NotNull String name
    );

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);

    void bindTaskById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void unbindTaskById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @NotNull List<TaskDto> findTasksByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

    void removeTasksByProjectId(
            @Nullable String projectId
    );
}
