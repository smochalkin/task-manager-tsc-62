package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Access denied...");
    }

}
