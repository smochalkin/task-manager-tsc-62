package ru.smochalkin.tm.constant;

public final class TerminalConst {

    public static final String ABOUT = "about";

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String INFO = "info";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String TASK_SHOW_BY_NAME = "task-show-by-name";

    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String TASK_START_BY_ID = "task-start-by-id";

    public static final String TASK_START_BY_NAME = "task-start-by-name";

    public static final String TASK_START_BY_INDEX = "task-start-by-index";

    public static final String TASK_COMPLETE_BY_ID = "task-complete-by-id";

    public static final String TASK_COMPLETE_BY_NAME = "task-complete-by-name";

    public static final String TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

    public static final String TASK_STATUS_UPDATE_BY_ID = "task-status-update-by-id";

    public static final String TASK_STATUS_UPDATE_BY_NAME = "task-status-update-by-name";

    public static final String TASK_STATUS_UPDATE_BY_INDEX = "task-status-update-by-index";

    public static final String TASK_BIND_BY_PROJECT_ID = "task-bind-by-project-id";

    public static final String TASK_UNBIND_BY_PROJECT_ID = "task-unbind-by-project-id";

    public static final String TASK_SHOW_BY_PROJECT_ID = "task-show-by-project-id";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String PROJECT_START_BY_ID = "project-start-by-id";

    public static final String PROJECT_START_BY_NAME = "project-start-by-name";

    public static final String PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String PROJECT_COMPLETE_BY_ID = "project-complete-by-id";

    public static final String PROJECT_COMPLETE_BY_NAME = "project-complete-by-name";

    public static final String PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";

    public static final String PROJECT_STATUS_UPDATE_BY_ID = "project-status-update-by-id";

    public static final String PROJECT_STATUS_UPDATE_BY_NAME = "project-status-update-by-name";

    public static final String PROJECT_STATUS_UPDATE_BY_INDEX = "project-status-update-by-index";

    public static final String EXIT = "exit";

}
