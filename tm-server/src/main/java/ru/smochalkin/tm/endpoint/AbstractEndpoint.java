package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.ServiceLocator;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected ISessionService sessionService;

}
